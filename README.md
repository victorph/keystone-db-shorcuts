# KEYSTONE-DB-SHORTCUTS #

# loading the library.

```
var db = require('keystone-db-shortcuts');
```

# db.get

## db.get as findById

### modelID Array or String
```
var query = {
  model:'modelName',
  id: modelID
};
db.get(query, callback(err, res){
  // do something
});
```
## db.get - Find by multiple ids.
id accepts an Array of values.
### modelID Array or String
```
var query = {
  model:'modelName',
  id: [...]
};
db.get(query, callback(err, res){
  // do something
});
```

## db.get as findOne (passing a where option.)
```
var query = {
  model:'modelName',
  where: { ... }, // list of columns
  one: true
};
db.get(query, callback(err, res){
  // do something
});
```

## db.get as find (passing a where option.)
```
var query = {
  model:'modelName',
  where: { ... } // list of columns
};
db.get(query, callback(err, res){
  // do something
});
```

## db.create
```
var query = {
  model:'modelName',
  body: { ... } // list of columns
};
db.get(query, callback(err, res){
  // do something
});
```

## db.update (it's like using get.)
```
var query = {
  model: 'modelName',
  where: { ... } // list of columns to use to find the record.
  body: { ... } // list of columns to change in the record.
};
db.update(query, callback(err, res){
  // do something
});
```

## db.destroy (it's like using get.)
```
var query = {
  model: 'modelName',
  where: { ... } // list of columns
};
db.destroy(query, callback(err, res){
  // do something
});
```

## License (MIT)

Copyright (c) 2014 Victor Peña <[http://www.victorph.com](http://www.victorph.com)>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.