var keystone = require('keystone'),_=require('lodash'),db=function keystoneDBShortcuts(){};
db.get=function get(o,cb){
	if(typeof cb===undefined)throw new Error('o.cb is not defined.');
	var m,q;
	m = keystone.list(o.model).model;
	if(!_.isUndefined(o.id)){
		if(_.isString(o.id))q=m.findById(o.id);
		else if(_.isArray(o.id))q=m.find({_id:{$in:o.id}});
		else if(_.isNumber(o.id)&&!_.isNaN(o.id))q=m.findById(o.id);
		else if(_.isFunction(o.id)){
			var _this=!_.isUndefined(o.this)&&_.isPlainObject(o.this)?o.this:o;
			q=m.find(o.id.bind(_this).call());
		}
	} 
	if(!_.isUndefined(o.where)){
		if(!_.isUndefined(o.one)){
			if(_.isFunction(o.where)){
				var _this=!_.isUndefined(o.this)&&_.isPlainObject(o.this)?o.this:o;
				q=m.findOne(o.where.bind(o).call());
			}
			if(_.isString(o.where))q=thingModel.findOne(o.where);
		}
		else{
			if(_.isFunction(o.where))q=m.find(o.where.bind(o).call());
			if(_.isString(o.where))q=m.find(o.where);
		}
	}
	if(!_.isUndefined(o.select)) {
		if(_.isArray(o.select))q.select(o.select.join(' '));
		else if(_.isString(o.select))q.select(o.select);
		else if(_.isFunction(o.select)).constructor)q.select(o.select(m));
	}
	q.exec(function(err,res){
		var _out=[];
		if(err){_out.push(err,null);}
		else{_out.push(null,res);}
		cb.apply(null,_out);
	});
};
db._save=function(o,cb){
	o.model.save(cb);
};
db.destroy=function destroy(o,cb){
	if(_.isString(o.model)){
		db.get(o,function(err,res){
			if(!err&&res)res.remove(cb);
			if(err&&res)cb(null,null);
			if(err)cb(err,null);
		});
	}
	else o.model.remove(cb);
};
db.update=function update(o,cb){
	if(_.isString(o.m)){
		db.get(o,function(err,res){
			if(err===null){
				res=_.merge(res,o.body);
				db._save(res,cb);
			}
			if(err&&res)cb(null,null);
			if(err !== null) cb(err, null);
		});
	} 
	else {
		o.m=_.merge(o.m,o.body);
		db._save(o.m,cb);
	}
};
db.create=function create(o,cb){
	var _thing,_new_thing;
	_thing=keystone.list(o.model);
	_new_thing=new _thing.model();
	_new_thing=_.merge(_new_thing,o.body);
	_new_thing.save(cb);
};
exports = module.exports = db;